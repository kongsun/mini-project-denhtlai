$(document).ready(function () {
    $("#btnLogin").on('click', function () {
        var number = $("#txtNumber").val();
        if (number == "679798") {
            window.location.replace("profile.html");
        } else {
            document.getElementById("error").innerHTML = "Wrong Phone number";
        }
    });

    //Time one
    var time1 = $(".time1");
    // Set the date we're counting down to
    var countDownDate1 = new Date("june 23, 2018 12:30:20").getTime();

    // Update the count down every 1 second
    var x1 = setInterval(function () {

        // Get todays date and time
        var now1 = new Date().getTime();

        // Find the distance between now an the count down date
        var distance1 = countDownDate1 - now1;

        // Time calculations for days, hours, minutes and seconds
        var days1 = Math.floor(distance1 / (1000 * 60 * 60 * 24));
        var hours1 = Math.floor((distance1 % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes1 = Math.floor((distance1 % (1000 * 60 * 60)) / (1000 * 60));
        var seconds1 = Math.floor((distance1 % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        time1.text(days1 + "d " + hours1 + "h " +
            minutes1 + "m " + seconds1 + "s ");

        // If the count down is over, write some text 
        if (distance1 < 0) {
            clearInterval(x1);
            time1.text = "Action End!";
        }
    }, 1000);

    //Time two
    var time = $(".time");
    // Set the date we're counting down to
    var countDownDate = new Date("june 25, 2018 15:37:25").getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"
        time.text(days + "d " + hours + "h " +
            minutes + "m " + seconds + "s ");

        // If the count down is over, write some text 
        if (distance < 0) {
            clearInterval(x);
            time.text = "Action End!";
        }
    }, 1000);


});